import { Injectable, CanActivate, ExecutionContext, UnauthorizedException, HttpException } from '@nestjs/common';
import { Observable } from 'rxjs';
import * as jwt from 'jsonwebtoken';
import { Reflector } from '@nestjs/core';
import { IUser } from 'src/models/user.models';
import { User } from 'src/db/user.entity';

@Injectable()
export class AuthGuard implements CanActivate {
  
    constructor(private reflector: Reflector){

    }
  
   async canActivate(
    context: ExecutionContext,
  ) {
    const request = context.switchToHttp().getRequest();
    try{
      const token = request.headers.jwt;
      const decoded:any = jwt.verify(token,'secret');

      const id = decoded.id;
      const user = await User.findOne({where: {id}});
      if(user){
          request.authUser = user;
      }
      return true
  }catch(e){
      throw new UnauthorizedException();
  }
  }
}