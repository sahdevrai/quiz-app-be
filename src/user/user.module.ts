import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/db/user.entity';
import { JwtModule } from '@nestjs/jwt';
import { QuizesService } from 'src/quiz/quiz.service';
import { QuizesController } from 'src/quiz/quiz.controller';
import { Quizes } from 'src/db/quizes.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([User,Quizes]),
    JwtModule.register({
        secret:'secret',
        signOptions:{expiresIn:'1d'}
    })
  ],
  providers: [UserService,QuizesService],
  controllers: [UserController,QuizesController]
})
export class UserModule {}