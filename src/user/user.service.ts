import { HttpException, Injectable, Response } from '@nestjs/common';
import { FindOptionsWhere, ObjectID, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../db/user.entity';
import { UpdateResult, DeleteResult } from  'typeorm';
import * as jwt from 'jsonwebtoken'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
) { }


  async  create(user: User | any): Promise<User> {
  
    const newUser= await this.userRepository.findOne({where: {email:user.email}});

    if(newUser)
    {
       throw new HttpException('User already registered',400)
    }
    else {
      return await this.userRepository.save(user);
    }
  }

  async userLogin({ email}:{ email: string;}):Promise<User> {
    const user= await this.userRepository.findOne({where: {email:email}});
  
    if(!user)
    {
      throw new HttpException('invalid credetials',400)
    }
    else {
      return user
    }
  }
  
}