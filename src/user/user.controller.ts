import { Controller, Get, Param, Post,Put, Delete, Body, UseGuards, BadRequestException, Res, Req, UnauthorizedException,  } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from '../db/user.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt'
import {Response, Request, response} from 'express';
import { request } from 'http';
import JwtUtil from 'src/utils/jwt.util';
import { AuthGuard } from 'src/guards/auth.guard';
import { ERole } from 'src/models/user.models';
import { Headers } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { schemaRegister,schemaLogin } from 'src/Joi/user.data';


@Controller('/user')
export class UserController {
  constructor(private readonly userService: UserService, private jwtService: JwtService) {}
  
  
  @Post('register')
    async register(@Body() userData: User): Promise<any> {
      let {name,email,password} =userData;

      name=name.trim()
      email=email.trim().toLowerCase()
      
      
      const result = schemaRegister.validate({name:name,email:email,password:password});

      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

      const hashedPassword= await bcrypt.hash(password,12)

      return this.userService.create({
        name:name,
        email:email,
        password:hashedPassword
      });
    }
  
    

  @Post('login')
    async userLogin(@Body() userData:User,
    @Res({passthrough:true}) response:Response)
    {

      let {email,password} =userData;

      email=email.trim().toLowerCase();
      

      const result = schemaLogin.validate({email:email,password:password})

        
        const { error } = result;
        if(error)
        {
          throw new BadRequestException(result.error.message)
        }

        const user=await  this.userService.userLogin({email});

        if(!user || !await bcrypt.compare(password,user.password))
        {
            throw new BadRequestException('invalid credentials')
        }

        const token = JwtUtil.getJwtToken(user)
      
      return {    
        jwt: token
      };
    }

    @UseGuards(AuthGuard)
  @Get('verify')
    async user(@Headers() headers) {
      try{
        const token = headers.jwt;
        const decoded:any = jwt.verify(token,'secret');
        const id = decoded.id;
        const user = await User.findOne({where: {id}});
        if(user){
            const {password, ...result} = user;

          return {result};
        }
      }
      catch(e){
        throw new UnauthorizedException();
      }
    }
  
}