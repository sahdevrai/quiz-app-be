import * as Joi from 'joi'


export const schemaRegister = Joi.object({
  email: Joi.string().email({ tlds: { allow: false } }).max(30).required(),
  password: Joi.string().min(8).max(30).required(),
  name: Joi.string().trim().min(3).max(20).required(),
});

export const schemaLogin = Joi.object({
  email: Joi.string().email({ tlds: { allow: false } }).required(),
  password: Joi.string().min(8).max(30).required()
})
