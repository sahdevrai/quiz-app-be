import * as Joi from 'joi';

export const schemaTitle = Joi.object({
  title: Joi.string().trim().min(5).max(30).required(),
});

export const schemaId = Joi.object({
  id: Joi.number().min(1).required(),
});


export const schemaQuestion = Joi.object({
  question: Joi.string().trim().min(15).max(150).required(),
  options: Joi.object({
    0:Joi.string().trim().min(1).max(40),
    1:Joi.string().trim().min(1).max(40),
    2:Joi.string().trim().min(1).max(40),
    3:Joi.string().trim().min(1).max(40),
    4:Joi.string().trim().min(1).max(40),
  }),
  typeOfQuestion: Joi.string().valid('single').valid('multiple').required(),

});

