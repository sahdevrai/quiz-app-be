import { Controller, Get, Param, Post,Put, Delete, Body, UseGuards, BadRequestException, Res, Req, UnauthorizedException,  } from '@nestjs/common';
import { QuizesService } from './quiz.service';
import { Quizes} from '../db/quizes.entity';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt'
import {Response, Request, response} from 'express';
import { AuthGuard } from 'src/guards/auth.guard';
import { Auth } from 'src/utils/auth.decorator';
import { ERole } from 'src/models/user.models';
import { Headers } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { User } from 'src/db/user.entity';
// import * as Joi from 'joi';
import { schemaId, schemaQuestion, schemaTitle } from 'src/Joi/quizes.data';



@Controller('/quiz')
export class QuizesController {
  constructor(private readonly quizesServices: QuizesService, private jwtService: JwtService) {} 

  @UseGuards(AuthGuard)
  @Get('myQuizes/:pg')
  async myQuizes(@Auth() auth,@Param('pg') pg,@Headers() headers): Promise<Quizes[]|any> {

    const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const id = decoded.id;

      const result = schemaId.validate({id:pg});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

    return this.quizesServices.myQuizes(id,pg);
  }

  @UseGuards(AuthGuard)
  @Post('create')
    async create(@Body() quizData: {title:string},@Headers() headers): Promise<any> {
      const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const id = decoded.id;

      let {title}=quizData

      title=title.trim()
      const result = schemaTitle.validate({title:title});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

      return this.quizesServices.create(title,id);
    }

  @Get('takeQuiz/:link/:id')
    async takeQuiz(@Param('link') link,@Param('id') id): Promise<any> {
      const result = schemaId.validate({id:id});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

      return this.quizesServices.takeQuiz(link,id);
    }
  @UseGuards(AuthGuard)
  @Get(':id/isAvailable')
    async isQuizAvailable(@Param('id') id,@Headers() headers): Promise<any> {
      const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const userId = decoded.id;

      const result = schemaId.validate({id:id});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }


      return this.quizesServices.isAvailable(userId,id);
    }

    @UseGuards(AuthGuard)
    @Put(':id/updateQuiz')
    async updateDetails(@Param('id') id, @Body() quizData: any,@Headers() headers): Promise<any> {

      const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const userId = decoded.id;
      const user = await User.findOne({where: {id:userId}})

      let {answer,Data}=quizData
      const result = schemaQuestion.validate(Data);
      const result1 = schemaId.validate({id:id});


      if(result1.error)
      {
        throw new BadRequestException(result.error.message)
      }


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }


      let {question,options,typeOfQuestion}=quizData

      question=question.trim();
      if(answer.forEach(element => {
        if([0,1,2,3,4].indexOf(element)==-1)
        return false
      })===false)
      {
        throw new BadRequestException("Provide valid answers")
      }
      
      let op={}
    const tempOptions=Object.keys(options).filter(value=>{
      if(options[value].trim()==='' && answer.indexOf(Number(value))!==-1)
      {
        //console.log(23)

        throw new BadRequestException('Answer cannot be an empty option')
        
      }
      if(options[value].trim()!=='')
      op={...op,[value]:options[value].trim()}
      return options[value].trim()!==''
    })
    if((typeOfQuestion==='single' && answer.length>1)||(answer.length>tempOptions.length))
    {
      throw new BadRequestException('Select valid number of options')
      
    }


    if(question.length<15 || tempOptions.length<=1 || answer.length===0)
    {
      throw new BadRequestException('Fill all the compulsory fields')
      
    }


let questionDetails={
  question:question,
  options:options,
  answer:answer,
  typeOfQuestion:typeOfQuestion}
        quizData.id = Number(id);
        
        //console.log(quizData)
        return this.quizesServices.update(quizData,id,userId,questionDetails);
    }

    @UseGuards(AuthGuard)
    @Get(':id/published')
    async published(@Param('id') id,@Headers() headers): Promise<any> {
     //console.log('reached')
      const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const userId = decoded.id;
      const user = await User.findOne({where:{id:userId}})
     //console.log('wewewe')
     const result = schemaId.validate({id:id});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

      return this.quizesServices.published(id,userId);
    } 

    @UseGuards(AuthGuard)
    @Delete(':id/delete')
    async delete(@Param('id') id,@Headers() headers): Promise<any> {
      const token = headers.jwt;
      const decoded:any = jwt.verify(token,'secret');
      const userId = decoded.id;
      const user = await User.findOne({where:{id:userId}})
     
      const result = schemaId.validate({id:id});


      const { error } = result;
      if(error)
      {
        throw new BadRequestException(result.error.message)
      }

      return this.quizesServices.delete(id,userId);
    }  

}