import { BadRequestException, HttpException, Injectable, Response, UnauthorizedException } from '@nestjs/common';
import { FindOptionsWhere, ObjectID, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateResult, DeleteResult } from  'typeorm';
import * as jwt from 'jsonwebtoken'
import { User } from 'src/db/user.entity';
import { Quizes } from 'src/db/quizes.entity';


@Injectable()
export class QuizesService {
  constructor(
    @InjectRepository(Quizes)
    private quizesRepository: Repository<Quizes>,
) { }

  async  myQuizes(id:any,pg:any):Promise<any>{
    
    let tempQuizes= await this.quizesRepository.find();
      tempQuizes=tempQuizes.filter((element)=>{
        return element.managedBy==id && element.published===true;
      })

     return [tempQuizes.slice((pg-1)*7,(pg-1)*7+7),Math.floor((tempQuizes.length-1)/7+1),tempQuizes.length];
  }

  async  takeQuiz(link:any,id:any):Promise<any>{
    let tempQuizes= await this.quizesRepository.findOne({where:{link:link}});

    if(tempQuizes===null)
    {
      throw new BadRequestException("Quiz doesn't exist")
    }

    if(id>JSON.parse(tempQuizes.questions).length || id<1)
    {
      throw new BadRequestException("Question with following id doesn't exist")
    }

    return {questionDetails:JSON.parse(tempQuizes.questions).slice(id-1,id),totalQuestions:JSON.parse(tempQuizes.questions).length,title:tempQuizes.title}
  }

  async  published(id:any,userId:any):Promise<any>{
    
    let tempQuizes= await this.quizesRepository.findOne({where:{id:id}});
    
    if(tempQuizes.managedBy!==userId)
    {
      throw new UnauthorizedException("You cannot access this quiz")
    }

    if(tempQuizes.published===true)
    {
      throw new BadRequestException("Quiz already published")
    }
    if(tempQuizes.questions.length===0)
    {
      throw new BadRequestException("Quiz doesn't have any question")
    }
    let characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let result = ""
    let charactersLength = characters.length;

    for ( var i = 0; i <=5 ; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
//console.log(tempQuizes)
    return await this.quizesRepository.update(id,{published:true,link:result});

     
  }

  async  isAvailable(userId:any,id:any){ 
    let tempQuizes= await this.quizesRepository.findOne({where:{id:id}});

    if(tempQuizes===null)
    {
      throw new BadRequestException("Quiz doesn't exist")
    }

    if(tempQuizes.managedBy!=userId)
    {
      throw new UnauthorizedException("You cannot access this quiz")
    }
    if(tempQuizes.published===true)
    {
      throw new BadRequestException("You cannot edit an already created quiz")
    }

    if(tempQuizes.questions==="")
    return 1
    else
    return JSON.parse(tempQuizes.questions).length+1
  }
  async  create(title: string,id:any){ 

    const isQuizNamed:any = await this.quizesRepository.findOne({where:{title:title}});
    //console.log(isQuizNamed)
    if(isQuizNamed!==null)
    {
    //console.log('isQuizNamed'+title)

      throw new BadRequestException("Another quiz is already named with simiar title, try somthing different")
    }

    await this.quizesRepository.save({managedBy:id,questions:"",link:"",title:title});
    //console.log('isQuizNamed1')

    let currentQuiz=await this.quizesRepository.findOne({where:{title:title}});
    //console.log('isQuizNamed2')

    //console.log(currentQuiz.id)
    //console.log('isQuizNamed3')

    return currentQuiz.id
}

async update(quizData:any,id,userId,questionDetails:Object): Promise<any> {

  const isQuizNamed:any = await this.quizesRepository.findOne({where:{id:quizData.id}});

  if(isQuizNamed===null)
  {
    //console.log('isQuizNamed')

    throw new BadRequestException("Quiz not initialised")
  }

  if(isQuizNamed!==null)
  {
    if(isQuizNamed.published===true)
    {
      throw new BadRequestException("Quiz already published")
    }
    if(isQuizNamed.managedBy!=userId)
    {
      throw new UnauthorizedException("You cannot access other person's quiz")
    }
    if(isQuizNamed.questions!=="")
    {
      if(JSON.parse(isQuizNamed.questions).length>=10)
      {
      throw new UnauthorizedException("Maximum question limit reached")
        
      }  
    }
  }

  let ans;
  //console.log(isQuizNamed)
  if(isQuizNamed.questions==="")
  {
    ans=[{...questionDetails}]
  }
  else{
    ans=[...JSON.parse(isQuizNamed.questions),{...questionDetails}]
  }
  
    return await this.quizesRepository.update(id,{questions:JSON.stringify(ans)});
}

  async delete(id: any,userId:any): Promise<DeleteResult> {
    const quiz:any = await this.quizesRepository.findOne({where:{id}});
    
    if(userId!==quiz.managedBy)
    {
      throw new UnauthorizedException("You cannot access this quiz")
    }

    return await this.quizesRepository.delete(id);
  }
}