import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, OneToMany } from 'typeorm';


@Entity()
export class Quizes
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;
    
    @Column({nullable:true,default:null})
    link: string;

    @Column({nullable:true,default:null})
    questions: string;    

    @Column({default:false})
    published: boolean;

    @Column()
    managedBy: number;

    toJSON(){
        return {
            id: this.id,
            title: this.title,
            link: this.link,
            questions: this.questions,
            published: this.published,
            managedBy: this.managedBy,

        };
      }
}